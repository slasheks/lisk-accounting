#!/usr/bin/env python3
import sys
import json
import datetime
import getpass
import argparse
try:
    import psycopg2
except ImportError:
    msg = 'Please install psycopg2. sudo pip3 install psycopg2'
    sys.exit(msg)

def get_config(config_path):
    """
    """
    configuration_items = {
        'db': {
            'database': 'lisk_main',
            'user': getpass.getuser(),
            'host': 'localhost',
            'password': 'password'
        }
    }

    connect_str = "dbname='{database}' user='{user}' host='{host}' password='{password}'"
    conn = psycopg2.connect(connect_str.format(**configuration_items['db']))

    return conn

def get_blocks(conn, pubkey):
    #Full blocks by generator
    sql_query = '''
        SELECT f."b_id" AS block_id, f."b_height" AS height, f."b_reward" AS reward,
        f."b_timestamp" AS timestamp, f."b_generatorPublicKey" AS public_key
        FROM blocks_list AS f
        WHERE f."b_generatorPublicKey" = %(pkey)s
        ORDER BY f."b_height" ASC
        '''

    sql_query_args = {'pkey': pubkey}

    cursor = conn.cursor()
    cursor.execute(sql_query, sql_query_args)
    column_names = [desc[0] for desc in cursor.description]

    results = []
    for row in cursor:
        new_row = [x.tobytes().decode() if isinstance(x, memoryview) else x for x in row]
        results.append(dict(zip(column_names, new_row)))

    #print(json.dumps({'data': results}, indent=3))
    return {'blocks': results}

def filter_blocks(blocks, filter_date):
    """
    """

    parsed = []
    exception_counter = 0

    for block in blocks['blocks']:

        try:
            new_timestamp = int(block['timestamp']) + 1464109200
        except TypeError:
            exception_counter += 1
            new_timestamp = 0 + 1464109200

        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d-%H:%M:%S')

        if filter_date:
            if not date.startswith(filter_date):
                continue

        parsed.append(block)

    return {'blocks': parsed}


def block_output(forged_blocks, output_type):
    """

    """
    if output_type == 'normal':
        fmt = '{} {} {} {}'
    elif output_type == 'csv':
        fmt = '{},{},{},{}'
    elif output_type == 'json':
        print(json.dumps(txobj))
        sys.exit()

    print(fmt.format('date', 'block_id', 'height', 'reward'))

    for block in forged_blocks['blocks']:

        try:
            new_timestamp = int(block['timestamp']) + 1464109200
        except TypeError:
            new_timestamp = 0 + 1464109200
        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d-%H:%M:%S')
        total_forged = int(block['reward']) / 100000000

        print(fmt.format(date, block['block_id'], block['height'], total_forged))


def main(pubkey, output_type, config_path, filter_date):
    """

    """
    conn = get_config(config_path)

    blocks = get_blocks(conn, pubkey)

    parsed_blocks = filter_blocks(blocks, filter_date)

    block_output(parsed_blocks, output_type)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(usage='soon')

    parser.add_argument('-d', '--delegate', dest='delegate', action='store',
                        help='Delegate account')

    parser.add_argument('-c', '--config-path', dest='config_path', action='store',
                        help='')

    parser.add_argument('--date', dest='date', action='store',
                        help='Date filter')

    parser.add_argument('-o', '--output', dest='output_type', action='store',
                        choices=('normal', 'csv', 'json'),
                        default='normal', help='Output type')

    args = parser.parse_args()

    main(args.delegate, args.output_type, args.config_path, args.date)
