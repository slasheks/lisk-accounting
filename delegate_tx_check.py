#!/usr/bin/env python

import re
import sys
import requests
import json
import math
import datetime
import argparse
import pylisk


def name_change(api, delegate):

    # if not an address then it is a name
    # try to convert to an address
    if not re.search(r'^\d+L$', delegate):

        payload = {'parameters':'/get?username={}'.format(delegate)}
        delegate_info = api.delegates('delegate_list',payload)
        return delegate_info['delegate']['address']

    else:

        return delegate

def static_name_mapping():
    """

    """
    try:

        with open('./name_map.json') as nmfh:

            name_map = json.load(nmfh)

            return name_map

    except IOError:

        sys.exit('Could not open name_map.json')

    pass

def paginate_transactions(api, call):
    """
    This will paginate calls with results higher than 100
    Args:
        api (class) - pylisk class for calls
        call (str)  - call that will be appended send or receive
    Returns:
        all_transactions (list) - all tx from an account
    """
    all_transactions = []
    tx_counter = 0

    # Need to make a call to get the count
    payload = {
        'parameters': '{}&orderBy=height&limit=1'.format(call),
        'id': None
    }
    transactions = api.transactions('blocktx', payload)
    tx_count = int(transactions['count'])
    tx_iterations = tx_count / 101
    remaining_txs = tx_count % 101

    if remaining_txs:

        tx_iterations += 1

    for i in xrange(0, tx_iterations):

        req = '&offset={}&orderBy=height'.format(tx_counter)
        new_call = call + req
        payload = {
            'parameters': new_call,
            'id': None
        }

        transactions = api.transactions('blocktx', payload)

        all_transactions += transactions['transactions']

        tx_counter += 101
        new_call = ''

    return all_transactions

def get_transactions(api, account):
    """
    There is a limit on the amount of transactions, need to get all
    """
    calls = ['?senderId={}'.format(account), '?recipientId={}'.format(account)]

    all_transactions = []

    for call in calls:

        all_transactions += paginate_transactions(api, call)
        
    tx_sorted = sorted(all_transactions, key=lambda k: k['timestamp'])

    return tx_sorted

def tx_parse(api, transactions, filter_date):
    """

    """

    txobj = {'data': []}
    name_map = static_name_mapping()

    transaction_types = {
        0: "Normal transaction",
        1: "2nd sig creation",
        2: "Delegate registration",
        3: "Delegate vote",
        4: "Multi-sig creation",
        5: "Dapp registration",
        6: "?",
        7: "Dapp deposit",
        8: "Dapp withdrawal"}

    transaction_fees = {
        0: 0.1,
        1: 5,
        2: 25,
        3: 1,
        4: "",
        5: "",
        6: "",
        7: "",
        8: ""}

    for tx in transactions:

        new_timestamp = tx['timestamp'] + 1464109200
        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d %H:%M:%S')

        if filter_date:

            if not date.startswith(filter_date):

                continue

        # Check if it exists
        if any(x['id'] == tx['id'] for x in txobj['data']):
            continue

        tx_amount = tx['amount'] / 100000000

        # TODO check a static file before making an api call

        # Get the name of the sender
        try:

           from_delegate_name = name_map[tx['senderId']]

        except KeyError:

            payload = {'parameters': '/get?publicKey={}'.format(tx['senderPublicKey'])}
            name = api.delegates('delegate_list', payload)

            if name['success']:
                from_delegate_name = name['delegate']['username']
            else:
                from_delegate_name = 'n/a'

        # Get the name of the recipient

        try:
            to_delegate_name = name_map[tx['recipientId']]
        except KeyError:
            payload = {'address': tx['recipientId']}
            rec_name = api.account('account', payload)

            try:
                rec_name_pubkey = rec_name['account']['publicKey']
                payload = {'parameters': '/get?publicKey={}'.format(rec_name_pubkey)}
                rec_name_data = api.delegates('delegate_list', payload)
                if rec_name_data['success']:
                    to_delegate_name = rec_name_data['delegate']['username']
                else:
                    to_delegate_name = 'n/a'
            except TypeError:
                rec_name_pubkey = 'n/a'
                to_delegate_name = 'n/a'

        txobj['data'].append({'fromAddr': tx['senderId'],
                              'fromName': from_delegate_name,
                              'toAddr': tx['recipientId'],
                              'toName': to_delegate_name,
                              'amount': tx_amount,
                              'fee': transaction_fees[tx['type']],
                              'id': tx['id'],
                              'type': transaction_types[tx['type']],
                              'date': date
                             })

    if not txobj['data']:

        print "No transactions found for this request"
        sys.exit()

    return txobj

def tx_print(tx_obj, output_type):
    """
    Print transactions based on output type
    Args:
        tx_obj (dict) - transaction information needed
        output_type (str) - How to output (ex: csv)
    Returns:
        None
    Output:
        Formatted transactions
    """

    output = []

    if output_type == 'normal':
        fmt = '{:21} {:19} {:21} {:19} {:6} {:4} {:20} {:22} {}'
    elif output_type == 'csv':
        fmt = '{},{},{},{},{},{},{},{},{}'
    elif output_type == 'json':
        print json.dumps(txobj)

    print fmt.format('from', 'name', 'to', 'name',
                     'amount', 'fee', 'id', 'type', 'date')

    for tx in tx_obj['data']:


        print fmt.format(tx['fromAddr'], tx['fromName'], tx['toAddr'],
                         tx['toName'], tx['amount'], tx['fee'], tx['id'],
                         tx['type'], tx['date'])

        output.append(fmt.format(tx['fromAddr'], tx['fromName'], tx['toAddr'],
                                 tx['toName'], tx['amount'], tx['fee'], tx['id'],
                                 tx['type'], tx['date']))

    #for line in set(output):
    #    print line

def get_delegates(api):
    """

    """

    payload = {'parameters': '?orderBy=rate:asc'}

    delegates = api.delegates('delegate_list', payload)

    return delegates['delegates']

def delegates_parse(delegates, output_type):
    """

    """

    output = []

    if output_type == 'normal':
        fmt = '{} {} {} {} {} {} {} {}'
    elif output_type == 'csv':
        fmt = '{},{},{},{},{},{},{},{}'
    elif output_type == 'json':
        print json.dumps(delegates)

    print fmt.format('username', 'producedblocks', 'missedblocks',
                     'productivity', 'rate', 'address', 'vote',
                     'approval')

    for d in delegates:

        output.append(fmt.format(d['username'], d['producedblocks'],
                                 d['missedblocks'], d['productivity'],
                                 d['rate'], d['address'], d['vote'],
                                 d['approval']))

def get_delegate_info(api, delegate):
    """

    """

    payload = {'parameters':'/get?username={}'.format(delegate)}

    delegate_info = api.delegates('delegate_list', payload)

    return delegate_info

def get_blocks_forged(api, delegate_info):
    """

    """

    pubkey = delegate_info['delegate']['publicKey']

    # Need to get the last forged and offset by x
    parameters = '?generatorPublicKey={}&limit=1&orderBy=height:desc'.format(pubkey)
    payload = {'parameters': parameters}
    last_forged_block = api.blocks('all_blocks', payload)

    forged_count = last_forged_block['count']
    offset = forged_count - 50

    parameters = '?generatorPublicKey={}&offset={}&orderBy=height' \
                 .format(pubkey, offset)
    payload = {'parameters': parameters}
    forged_blocks = api.blocks('all_blocks', payload)

    return forged_blocks

def block_parser(forged_blocks):
    """

    """

    fmt = '{} {} {} {}'

    for block in forged_blocks['blocks']:

        new_timestamp = block['timestamp'] + 1464109200
        date = datetime.datetime.fromtimestamp(new_timestamp).strftime('%Y-%m-%d %H:%M:%S')

        total_forged = int(block['totalForged']) / 100000000

        print date, block['height'], total_forged, int(block['totalForged'])


def main():
    """

    """

    api = pylisk.liskAPI(args.url)

    delegate = name_change(api, args.delegate)

    if args.mode == 'tx_per_delegate':

        transactions = get_transactions(api, delegate)

        tx_obj = tx_parse(api, transactions, args.date)

        tx_print(tx_obj, args.output_type)

    elif args.mode == 'stats_101':

        delegates = get_delegates(api)

        delegate_list = delegates_parse(delegates, args.output_type)

    elif args.mode == 'forged_blocks':

        # Need to get the pubkey of the user (autoname)
        delegate_info = get_delegate_info(api, delegate)

        # Need to query the blocks forged by pubkey
        forged_blocks = get_blocks_forged(api, delegate_info)

        # there are many so need a range
        parsed_blocks = block_parser(forged_blocks)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(usage='soon')

    parser.add_argument('-d', '--delegate', dest='delegate', action='store',
                        help='Delegate account')

    parser.add_argument('--date', dest='date', action='store',
                        help='Date filter')

    parser.add_argument('-m', '--mode', dest='mode', action='store',
                        choices=('tx_per_delegate', 'stats_101',
                                 'forged_blocks'),
                        default='tx_per_delegate', help='Output type')

    parser.add_argument('-o', '--output', dest='output_type', action='store',
                        choices=('normal', 'csv', 'json'),
                        default='normal', help='Output type')

    parser.add_argument('-u', '--url', dest='url', action='store',
                        default='http://localhost:8000',
                        help='Url to make the requests')

    args = parser.parse_args()

    main()
