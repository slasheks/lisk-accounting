#!/usr/bin/env bash

./delegate_tx_check.py -d slasheks --date 2017 -o csv > slasheks
./delegate_tx_check.py -d slasheks_voting --date 2017 -o csv > slasheks_voting
./delegate_tx_check.py -d slasheks_app_fund --date 2017 -o csv > slasheks_app_fund
./delegate_tx_check.py -d slasheks_bug_bounty --date 2017 -o csv > slasheks_bug_bounty
./delegate_tx_check.py -d liska --date 2017 -o csv > liska
./delegate_tx_check.py -d 5666375702678662313L --date 2017 -o csv > 5666375702678662313L
